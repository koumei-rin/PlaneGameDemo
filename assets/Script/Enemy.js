cc.Class({
    extends: cc.Component,

    properties: {
        // 暂存 enemyManager 对象的引用
        enemyManager: {
            default: null,
            serializable: false
        },
		
		// alive timer
		timer : 0,
		// alive max time in second
		aliveMaxTime : 4,
		
		// speed
		speed : 500,
		// hp
		maxHp : 5,
    },

    // use this for initialization
    onLoad: function () {
    },
	
    init: function (enemyManager) {
		if (enemyManager) {
			this.enemyManager = enemyManager;
		}
		this.alive = true;
		this.timer = 0;
		this.hp = this.maxHp;
    },

    unuse: function () {
		this.enemyManager = null;
    },

    reuse: function (enemyManager) {
		this.enemyManager = enemyManager; // get 中传入的管理类实例
    },
	
	hit : function (attack) {
		if (attack) {
			this.hp -= attack;
		} else {
			this.hp--;
		}
		if (this.hp <= 0) {
			this.alive = false;
			this.enemyManager.put(this.node);
		}
	},
	
	onCollisionEnter: function (other, self) {
		cc.log('enemy : on collision enter');
		if (this.alive) {
			this.hit();
		}
	},

    // called every frame, uncomment this function to activate update callback
    update: function (dt) {
		if (!this.alive) return ;
		// update timer
		this.timer += dt;
		// 如果在生存周期之内
		if (this.timer < this.aliveMaxTime) {
			this.node.y -= this.speed * dt;
		} else {
			this.alive = false;
			this.enemyManager.put(this.node);
		}
    },
});
