let Collider = require("Collider");

cc.Class({
    extends: cc.Component,

    properties: {
        // collider type : 使用于碰撞检测时，根据碰撞类型执行不同的动作 
        COLLIDER_TYPE: {
            visible: false,
            default: Collider.type.PLAYER,
            type: cc.Integer,
        },

		// liner speed
		linerSpeed : 500,
		// max move speed
		maxMoveSpeed : 60,
		// ability
		maxHp : 3,
    },

    // use this for initialization
    onLoad: function () {
		// disable player
		this.enabled = false;
        // 主角当前方向速度
        this.xSpeed = 0;
        this.ySpeed = 0;
        // 主角当前方向向量
		this.directLiner = cc.p(0, 0);
        // 初始化键盘输入监听
        this.setInputControl();
		
		// ability
		this.alive = true;
		this.hp = this.maxHp;
    },
	
	init: function () {
		this.enabled = true;
		this.alive = true;
		this.hp = this.maxHp;
	},
	
	onCollisionEnter: function (other, self) {
		if (this.alive) {
            let ocpt = other.node.getComponent(other.node.name);
            if (ocpt.COLLIDER_TYPE === Collider.type.ENEMY) {
                this.hit();
            } else if (ocpt.COLLIDER_TYPE === Collider.type.ENEMY_BULLET) {
                this.hit();
            }
		}
	},
	
	hit: function (attack) {
		if (attack) {
			this.hp -= attack;
		} else {
			this.hp--;
		}
		if (this.hp <=0) {
			this.alive = false;
		}
	},

	// 键盘输入监听
	setInputControl: function () {
        var self = this;
        //add keyboard input listener to jump, turnLeft and turnRight
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            // set a flag when key pressed
            onKeyPressed: function(keyCode, event) {
                switch(keyCode) {
                    case cc.KEY.a:
                    case cc.KEY.left:
                        self.directLiner.x = self.directLiner.x - self.linerSpeed < -self.linerSpeed ? -self.linerSpeed : self.directLiner.x - self.linerSpeed;
                        break;
                    case cc.KEY.d:
                    case cc.KEY.right:
                        self.directLiner.x = self.directLiner.x + self.linerSpeed > self.linerSpeed ? self.linerSpeed : self.directLiner.x + self.linerSpeed;
                        break;
                    case cc.KEY.w:
                        self.directLiner.y = self.directLiner.y + self.linerSpeed > self.linerSpeed ? self.linerSpeed : self.directLiner.y + self.linerSpeed;
                        break;
                    case cc.KEY.s:
                        self.directLiner.y = self.directLiner.y - self.linerSpeed < -self.linerSpeed ? -self.linerSpeed : self.directLiner.y - self.linerSpeed;
                        break;
                }
            },
            // unset a flag when key released
            onKeyReleased: function(keyCode, event) {
                switch(keyCode) {
                    case cc.KEY.a:
                    case cc.KEY.left:
                        self.directLiner.x = self.directLiner.x + self.linerSpeed > self.linerSpeed ? self.linerSpeed : self.directLiner.x + self.linerSpeed;
                        break;
                    case cc.KEY.d:
                    case cc.KEY.right:
                        self.directLiner.x = self.directLiner.x - self.linerSpeed < -self.linerSpeed ? -self.linerSpeed : self.directLiner.x - self.linerSpeed;
                        break;
                    case cc.KEY.w:
                        self.directLiner.y = self.directLiner.y - self.linerSpeed < -self.linerSpeed ? -self.linerSpeed : self.directLiner.y - self.linerSpeed;
                        break;
                    case cc.KEY.s:
                        self.directLiner.y = self.directLiner.y + self.linerSpeed > self.linerSpeed ? self.linerSpeed : self.directLiner.y + self.linerSpeed;
                        break;
                }
            }
        }, self.node);

        // touch input
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            onTouchBegan: function(touch, event) {
                var touchLoc = touch.getLocation();
                if (touchLoc.x >= cc.winSize.width/2) {
                    self.directLiner.x = false;
                    self.directLiner.y = true;
                } else {
                    self.directLiner.x = true;
                    self.directLiner.y = false;
                }
                // don't capture the event
                return true;
            },
            onTouchEnded: function(touch, event) {
                self.directLiner.x = false;
                self.directLiner.y = false;
            }
        }, self.node);
    },
	
    // called every frame, uncomment this function to activate update callback
    update: function (dt) {
        // 根据当前向量每帧更新速度
        this.xSpeed = this.directLiner.x * dt;
        this.ySpeed = this.directLiner.y * dt;
		
        // 限制主角的速度不能超过最大值
        if ( Math.abs(this.xSpeed) > this.maxMoveSpeed ) {
            // if speed reach limit, use max speed with current direction
            this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
        }
        if ( Math.abs(this.ySpeed) > this.maxMoveSpeed ) {
            // if speed reach limit, use max speed with current direction
            this.ySpeed = this.maxMoveSpeed * this.ySpeed / Math.abs(this.ySpeed);
        }

        // 根据当前速度更新主角的位置
        this.node.x += this.xSpeed;
        this.node.y += this.ySpeed;
		
		// limit player position inside screen
        if ( this.node.x > this.node.parent.width/2) {
            this.node.x = this.node.parent.width/2;
            this.xSpeed = 0;
        } else if (this.node.x < -this.node.parent.width/2) {
            this.node.x = -this.node.parent.width/2;
            this.xSpeed = 0;
        }
        if ( this.node.y > this.node.parent.height/2) {
            this.node.y = this.node.parent.height/2;
            this.ySpeed = 0;
        } else if (this.node.y < -this.node.parent.height/2) {
            this.node.y = -this.node.parent.height/2;
            this.ySpeed = 0;
        }
    },
});
