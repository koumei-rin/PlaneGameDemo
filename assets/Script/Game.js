var BulletManager = require("BulletManager");
var EnemyManager = require("EnemyManager");
var Player = require("Player");

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
		
		// ui
        startBtn: {
            default: null,
            type: cc.Node
        },
		
		// 暂存 bullet manager 对象的引用
        bulletManager : {
            default: null,
            type: cc.Node
        },
        enemyManager : {
            default: null,
            type: cc.Node
        },
		
        title : {
            default: null,
            type: cc.Label
        },
		
        player : {
            default: null,
            type: Player
        },
		
    },

    // use this for initialization
    onLoad: function () {
		// 获取碰撞检测系统
		var manager = cc.director.getCollisionManager();
		// 默认碰撞检测系统是禁用的，如果需要使用则需要以下方法开启碰撞检测系统
		manager.enabled = true;
		// 默认碰撞检测系统的 debug 绘制是禁用的，如果需要使用则需要以下方法开启 debug 绘制
		manager.enabledDebugDraw = true;
		
		// bullet manager
		//this.bulletManager.hello();
		var bm = this.bulletManager.getComponent(BulletManager);
		var em = this.enemyManager.getComponent(EnemyManager);
		if (bm || em) {
			bm.init(this);
			em.init(this);
        }
        else {
            cc.error("Something wrong?");
        }
		
		// timer
		this.timer = 0;
		// set game state
		this.isRunning = false;
		// set title
		this.title.string = 'Game?';
		
		// test
		//this.startGame();
    },
	
	gameStart: function () {
        // set game state to running
        this.isRunning = true;
		// set title
		this.title.string = "Game Start!";
        // set button and gameover text out of screen
        this.startBtn.setPositionX(3000);
		// pause player controller
		cc.eventManager.resumeTarget(this.player.node);
		this.player.init();
	},
	
	gameOver: function () {
		// set game state
		this.isRunning = false;
		// set title
		this.title.string = "Game Over!";
        // set button and gameover text out of screen
        this.startBtn.setPositionX(0);
		// pause player controller
		this.player.enabled = false;
		//this.player.stopMove();
		cc.eventManager.pauseTarget(this.player.node);
		this.player.node.stopAllActions();
		// move player to screen center
		this.player.directLiner = cc.p(0,0);
		this.player.node.runAction(cc.moveTo(1, cc.p(0, -200)));
	},

    // called every frame, uncomment this function to activate update callback
    update: function (dt) {
		// is game over
		if (!this.isRunning) return ;
		
		// game over
		if (!this.player.alive) {
			this.gameOver();
			return ;
		}
		
		// update timer
		this.timer += dt;
		
		if (this.timer > 0.3) {
			// reset timer
			this.timer = 0;
			// create bullet
			let bm = this.bulletManager.getComponent(BulletManager);
			let em = this.enemyManager.getComponent(EnemyManager);
			if (bm || em) {
				bm.create();
				em.create();
			} else {
				cc.error("Something wrong?");
			}
		}
		
    },
});
