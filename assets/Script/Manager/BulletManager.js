cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        
        bulletPrefab : {
            default : null,
            type : cc.Prefab,
        },
        
        // 暂存 Game 对象的引用
        game: {
            default: null,
            serializable: false
        },
        
        // 保存对象
        objs: [],
    },

    // use this for initialization
    onLoad: function () {
        cc.log("bullet manager : onload()");
    },

    // called every frame, uncomment this function to activate update callback
    update: function (dt) {

    },
    
    hello : function () {
        cc.log("hello");
    },
    
    init : function (game) {
        this.game = game;
        
        this.pool = new cc.NodePool('Bullet');
        let initCount = 20;
        for (let i = 0; i < initCount; ++i) {
            let bullet = cc.instantiate(this.bulletPrefab); // 创建节点
            this.pool.put(bullet); // 通过 putInPool 接口放入对象池
        }
        cc.log("bullet manager : init()");
    },
    
    create : function () {
        var bullet = null;
        // 使用给定的模板在场景中生成一个新节点
        if (this.pool.size() > 0) {
            bullet = this.pool.get(this); // parentNode will be passed to bullet's reuse method
        } else {
            bullet = cc.instantiate(this.bulletPrefab);
        }
        if (bullet) {
            // 保存对象，以便回收
            this.objs.push(bullet);
            // 设置对象的坐标
            // bullet.setPosition(cc.p(0, 0));
            bullet.getComponent('Bullet').init(this);
            bullet.setPosition(this.game.player.node);
            cc.director.getScene().getChildByName('Canvas').addChild(bullet);
        } else {
            cc.error("bullet manager has something wrong");
        }
    },
    
    put : function (bullet) {
        this.pool.put(bullet);
    },

    recoverAll: function () {
        // 遍历对象，回收对象
        for (i in this.objs) {
            this.pool.put(this.objs[i]);
        }
        // 清空缓存对象数组
        this.objs = [];
    },
});
