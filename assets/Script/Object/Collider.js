module.exports = {
    type: {
        PLAYER: 10000,
        PLAYER_BULLET: 100001,
        ENEMY: 11000,
        ENEMY_BULLET: 11001,
    },
};